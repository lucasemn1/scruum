import styled from 'vue-styled-components';
import { Container as DefaultContainer } from '@/pages/style';

export const Container = styled(DefaultContainer)`
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const CardsArea = styled.div`
  display: flex;

  & > .router-link:not(:first-child) {
    margin-left: 62px;
  }
`;

export const Card = styled.div`
  width: 250px;
  height: 250px;
  border-radius: 20px;
  background-color: #ffffff;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  box-shadow: 0px 4px 44px rgba(108, 109, 112, 0.04);
  transition: 0.1s;

  &:hover {
    background-color: #F0542D;
    transition: 0.1s;

    & > svg > path {
      stroke: #ffffff;
      fill: #ffffff;
    }

    & > h1 {
      color: #ffffff;
    }
  }

  &:active {
    background-color: #F9A31B;
  }
`;

export const CardTitle = styled.h1`
  font-size: 24px;
  color: #F0542D;
  margin-top: 10px;
  font-weight: 500;
  text-transform: uppercase;
`;