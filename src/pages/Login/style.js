import styled from 'vue-styled-components';
import DefaultInputIcon from '@/components/InputIcon/InputIcon.vue';
import { Container as DefaultContainer } from '@/pages/style';

export const Container = styled(DefaultContainer)`
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const FormLogin = styled.form`
  width: 717px;
  height: 300px;
  padding: 50px;
  background-color: #FFFFFF;
  border-radius: 20px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  box-shadow: 0px 4px 44px rgba(108, 109, 112, 0.04);
`;

export const Logo = styled.img`
  width: 190px;
  height: 148px;
`;

export const FormArea = styled.div`
  width: calc(100% - 230px);
`;

export const InputIcon = styled(DefaultInputIcon)`
  &:not(:first-child) {
    margin-top: 10px;
  }
`;

export const FormLoginButton = styled.button`
  width: 100%;
  height: 56px;
  background-color: #F0542D;
  border: none;
  text-transform: uppercase;
  color: #FFFFFF;
  border-radius: 10px;
  margin-top: 22px;
  transition: 0.2s;

  &:hover {
    transition: 0.2s;
    background-color: #F9A31B;
  }
`;