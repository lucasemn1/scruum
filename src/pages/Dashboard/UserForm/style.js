import styled from 'vue-styled-components';

export const UserForm = styled.form`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-column-gap: 48px;
  margin-top: 25px;
  background-color: var(--white);
  padding: 36px;
  border-radius: 12px;

  & > div {
    width: 100%;

    & > * {
      margin-bottom: 20px;
    }
  }
`;