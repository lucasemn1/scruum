import styled from 'vue-styled-components';

export const Container = styled.div`
  min-height: 100vh;
  width: 100%;
  padding: 30px;
`;