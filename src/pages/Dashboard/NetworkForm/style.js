import styled from 'vue-styled-components';

export const NetworkForm = styled.form`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-column-gap: 48px;
  margin-top: 25px;

  & > div {
    width: 100%;

    & > * {
      margin-bottom: 20px;
    }
  }
`;