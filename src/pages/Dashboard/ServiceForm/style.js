import styled from 'vue-styled-components';

export const ServiceForm = styled.form`
  display: grid;
  grid-template-columns: 500px;
  grid-column-gap: 48px;
  margin-top: 25px;

  & > div {
    width: 100%;

    & > * {
      margin-bottom: 20px;
    }
  }
`;