import styled from 'vue-styled-components';

export const StoreForm = styled.form`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-column-gap: 48px;
  margin-top: 25px;
  background-color: var(--white);
  border-radius: 12px;
  padding: 36px;

  & > div {
    width: 100%;

    & > * {
      margin-bottom: 20px;
    }
  }
`;