import router from '../router/index.js';

export function goTo(path, query) {
  if(path !== undefined) {
    router.push({ path, query }).catch(() => {});
  }
}

export function goBack(){
  router.back().catch(() => {});
}