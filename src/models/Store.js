export default class Store{
  id;
  name;
  cnpj;
  companyName;
  phone;
  client;
  cep;
  street;
  neighborhood;
  city;
  state;
  number;

  constructor(id, name, cnpj, phone, client, cep, street, neighborhood, city, state, number){
    this.id = id;
    this.name = name;
    this.cnpj = cnpj;
    this.phone = phone;
    this.client = client;
    this.cep = cep;
    this.street = street;
    this.neighborhood = neighborhood;
    this.city = city;
    this.state = state;
    this.number = number;
  }
}