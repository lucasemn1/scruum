export default class User{
  id;
  name;
  email;
  createdAt;
  status;
  phone;
  cpf;
  office;
  client;
  stores;
  services;

  constructor(id, name, email, createdAt, status, phone, cpf, office, client, stores, services){
    this.id = id;
    this.name = name;
    this.email = email;
    this.createdAt = createdAt;
    this.status = status;
    this.phone = phone;
    this.cpf = cpf;
    this.office = office;
    this.client = client;
    this.stores = stores;
    this.services = services;
  }
}