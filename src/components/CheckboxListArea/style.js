import styled from 'vue-styled-components';

export const CheckList = styled.div`
  width: 100%;
`;

export const AreaTitle = styled.h2`
  font-family: var(--font-secondary);
  font-size: 17px;
  margin-bottom: 12px;
  font-weight: normal;
`;

export const List = styled.div`
  display: flex;
  flex-wrap: wrap;
`;

export const ListItem = styled.div`
  margin-right: 24px;
  margin-bottom: 10px;
  display: flex;
  align-items: center;

  & > input {
    margin-right: 9px;
  }

  & > label {
    font-family: var(--font-secondary);
    font-style: normal;
    font-weight: normal;
    font-size: 15px;
    line-height: 22px;
    /* identical to box height, or 147% */

    display: flex;
    align-items: center;

    color: #646464;
  }
`;