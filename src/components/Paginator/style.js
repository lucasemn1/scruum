import styled from 'vue-styled-components'


export const Container = styled.div`
  display: flex;

  & button {
    width: 52px;
    height: 38px;
    border-radius: 10px;
    font-size: 16px;
    border: none;
    transition: background-color 100ms;
  }
`

export const NavigationButton = styled.button`
  background-color: var(--orange);
  color: var(--white);

  &:hover{
    background-color: var(--dark-orange);
  }
`

export const PageButton = styled.button`
  background-color: var(--gray-1);
  color: var(--gray-4);
  margin: 0 2px;

  &.active{
    color: var(--orange);
  }

  &:hover{
    background-color: var(--gray-2);
  }
`