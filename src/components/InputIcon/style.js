import styled from 'vue-styled-components';

export const InputContainer = styled.div`
  width: 100%;
  height: 56px;
  background-color: #F1F1F5;
  border-radius: 10px;
  padding: 0 20px;
  display: flex;
  align-items: center;
`;

export const IconInput = styled.img` 
  width: 30px;
  height: 30px;
  margin-right: 12.5px;
`;

export const CustomInput = styled.input`
  width: 100%;
  height: 100%;
  background-color: transparent;
  border: none;
  outline: none;
  font-size: 16px;
  padding: 0;
  
  &::placeholder {
    color: rgba(108, 109, 112, 0.5);
  }
`;