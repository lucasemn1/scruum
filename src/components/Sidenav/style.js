import styled from 'vue-styled-components';

export const Sidenav = styled.section`
  height: calc(100vh - 60px);
  position: fixed;
  width: 250px;
  background-color: var(--light-color);
  border-radius: 20px;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const Logo = styled.img`
  width: 94px;
  height: 73px;
  margin-top: 30px;
`;

export const NavMenu = styled.ul`
  width: 200px;
  list-style: none;
  margin-top: 30px;
`;

export const NavMenuItem = styled.li`
  width: 100%;
  height: 45px;
  display: flex;
  align-items: center;
  border-radius: 10px;
  text-align: left;
  font-weight: bold;
  font-size: 18px;
  cursor: pointer;
  transition: 0.1s;
  color: var(--gray-4);

  &:hover {
    color: var(--orange);
    transition: 0.1s;
  }
`;

export const NavMenuItemIcon = styled.img`
  width: 22.5px;
  height: 22.5px;
`;