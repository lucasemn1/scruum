import styled from 'vue-styled-components'
import { CustomButton } from '@/styles/buttons'

export const HeaderContainer = styled.header`
  background-color: var(--white);
  border-radius: 12px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  height: 60px;
  padding: 20px 30px;
  margin-bottom: 20px;
`

export const CustomInput = styled.input`
  width: 40%;
  height: 40px;
`

export const HeaderButton = styled(CustomButton)`
  width: auto;
  height: auto;
  font-size: 24px;
  padding: 8px 32px;
  transition: background-color 100ms;

  &:hover{
    background-color: var(--dark-orange);
  }
`