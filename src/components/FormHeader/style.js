import styled from 'vue-styled-components'

export const HeaderContainer = styled.header`
  height: 70px;
  background-color: var(--white);
  display: flex;
  padding: 0 32px;
  align-items: center;
  border-radius: 12px;
  justify-content: space-between;

  & > div {
    display: flex;
    align-items: center;
  }

  & img {
    width: 30px;
    height: 30px;
  }

  & img:hover{
    cursor: pointer;
  }

  & h1{
    margin-left: 20px;
  }
`

export const SubmitButton = styled.button`
  padding: 15px 30px;
  background-color: var(--orange);
  border-radius: 28px;
  color: var(--white);
  border: none;
`
