import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

export const paths = {
  home: '/home',
  login: '/login',
  dashboard: '/',
  analisys: '/analisys',
  collaborative: '/collaborative',
  shopping: '/shopping',
}

const routes = [
  // {
  //   path: '/',
  //   name: 'Home',
  //   component: Home
  // },
  {
    path: paths.login,
    name: 'Login',
    component: () => import('@/pages/Login/Login.vue')
  },
  {
    path: paths.home,
    name: 'Home',
    component: () => import('@/pages/Home/Home.vue')
  },
  {
    path: paths.dashboard,
    name: 'Dashboard',
    component: () => import('@/pages/Dashboard/Dashboard.vue'),
    children: [
      {
        path: '/form/user/:id?',
        name: 'user-form',
        component: () => import('@/pages/Dashboard/UserForm/UserForm.vue')
      },
      {
        path: '/form/store/:id?',
        name: 'store-form',
        component: () => import('@/pages/Dashboard/StoreForm/StoreForm.vue')
      },
      {
        path: '/form/network',
        name: 'network-form',
        component: () => import('@/pages/Dashboard/NetworkForm/NetworkForm.vue')
      },
      {
        path: '/form/service',
        name: 'service-form',
        component: () => import('@/pages/Dashboard/ServiceForm/ServiceForm.vue')
      },
      {
        path: '/users',
        name: 'users',
        component: () => import('@/pages/Dashboard/Users/Users.vue')
      },
      {
        path: '/stores',
        name: 'stores',
        component: () => import('@/pages/Dashboard/Stores/Stores.vue')
      }
    ]
  },
  {
    path: paths.analisys,
    name: 'Analisys',
    component: () => import('@/pages/Analisys/Analisys.vue')
  },
  {
    path: paths.collaborative,
    name: 'Collaborative',
    component: () => import('@/pages/Collaborative/Collaborative.vue')
  },
  {
    path: paths.shopping,
    name: 'Shopping',
    component: () => import('@/pages/Shopping/Shopping.vue')
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
})

export default router
