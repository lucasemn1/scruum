import styled from 'vue-styled-components';

export const CustomButton = styled.button`
  width: 310px;
  height: 49px;
  background-color: var(--primary-color);
  font-family: var(--font-primary);
  border: none;
  border-radius: 20px;
  font-size: 16px;
  outline: none;
  color: var(--light-color);
`;