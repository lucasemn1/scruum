import styled from 'vue-styled-components'

export const ContentContainer = styled.div`
  background-color: var(--white);
  border-radius: 12px;
  padding: 20px 4px;
  flex: 1;
  display: flex;
  flex-direction: column;
  align-items: center;
`