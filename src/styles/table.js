import styled from 'vue-styled-components'

export const TableContainer = styled.table`
  width: 100%;
  text-align: left;
  border-collapse: collapse;
  margin-bottom: 16px;

  & tr:nth-child(even){
    background-color: var(--gray-1)
  }

  & tr:hover{
    background-color: var(--gray-2);
  }

  & tr{
    cursor: pointer;
  }

  & td, & th{
    padding: 12px 32px;
  }
`