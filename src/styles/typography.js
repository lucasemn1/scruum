import styled from 'vue-styled-components';

export const PageTitle = styled.h1`
  color: var(--gray-4);
  font-size: 22px;
  font-weight: normal;
`;

export const HeaderPageTitle = styled.h1`
  color: var(--gray-4);
  font-size: 18px;
`